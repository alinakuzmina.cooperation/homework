fun main(args: Array<String>) {
    val valForString: String = "Что-то авторское!"
    val valForLong: Long = 3000000000
    val valForByte: Byte = 50
    val valForShot: Short = 200
    val valForDouble: Double = 0.666666667
    val valForInt: Int = 18500
    val valForFloat: Float = 0.5F

    println("Заказ - \'$valForString\' готов!")
    println("Заказ - \'$valForLong капель фреша\' готов!")
    println("Заказ - \'$valForByte мл виски\' готов!")
    println("Заказ - \'$valForShot мл пина колады\' готов!")
    println("Заказ - \'$valForDouble литра эля\' готов!")
    println("Заказ - \'$valForInt мл лимонада\' готов!")
    println("Заказ - \'$valForFloat литра колы\' готов!")
}